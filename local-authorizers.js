const AWS = require('aws-sdk');

module.exports.localAuthorizer = async (event, context) => {
  event.methodArn = event.methodArn.replace('random-api-id', process.env.SYSTEM_KEY);

  const lambda = new AWS.Lambda();
  const result = await lambda
    .invoke({
      FunctionName: 'alicerce-auth-dev-authorizer',
      InvocationType: 'RequestResponse',
      Payload: JSON.stringify(event)
    })
    .promise();
  if (result.StatusCode === 200) {
    return JSON.parse(result.Payload);
  }

  console.error(result.FunctionError);
  throw Error('Authorizer error');
};
