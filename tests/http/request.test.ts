import { createSandbox, createStubInstance, SinonSandbox } from 'sinon';
import * as typeorm from 'typeorm';
import { HandlerEvent } from '../../src/types/handler';
import { createStubConnection } from '../utils/testUtils';
import { getUserFromEvent, getPermissionsFromEvent, getAdminflagFromEvent } from '../../src/http/request';

describe('@http -> request', () => {
  let sandbox: SinonSandbox;

  beforeEach(() => {
    sandbox = createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('getUserFromEvent() --> retorna o usuário do authorizer', async () => {
    let dummyUser = new User();
    dummyUser.id = 1;
    dummyUser.first_name = 'teste';

    // mocks
    const fakeRepository = createStubInstance(typeorm.Repository);
    fakeRepository.findOne.withArgs(1 as any).resolves(dummyUser);
    createStubConnection(sandbox, { getRepository: fakeRepository });
    // fim mocks

    let dummyEvent = {
      pathParameters: {},
      headers: {},
      requestContext: {
        authorizer: {
          user: JSON.stringify(dummyUser)
        }
      }
    } as HandlerEvent;

    let user = (await getUserFromEvent(dummyEvent)) as User;
    expect(user.first_name).toBe('teste');

    delete dummyEvent.requestContext;
    user = (await getUserFromEvent(dummyEvent)) as User;
    expect(user).toBe(false);

    dummyEvent.requestContext = { authorizer: {} };
    user = (await getUserFromEvent(dummyEvent)) as User;
    expect(user).toBe(false);
  });

  it('getUserFromEvent() --> retorna false por nao encontrar usuario', async () => {
    let dummyUser = new User();
    dummyUser.id = 1;
    dummyUser.first_name = 'teste';

    // mocks
    const fakeRepository = createStubInstance(typeorm.Repository);
    fakeRepository.findOne.withArgs(1 as any).resolves(false);
    createStubConnection(sandbox, { getRepository: fakeRepository });
    // fim mocks

    let dummyEvent = {
      pathParameters: {},
      headers: {},
      requestContext: {
        authorizer: {
          user: JSON.stringify(dummyUser)
        }
      }
    } as HandlerEvent;

    let res = (await getUserFromEvent(dummyEvent));
    expect(res).toBe(false);
  });

  it('getPermissionsFromEvent() --> retorna as permissões do authorizer', () => {
    let dummyEvent = {
      pathParameters: {},
      headers: {},
      requestContext: {
        authorizer: {
          permissions: JSON.stringify(['1', '2'])
        }
      }
    } as HandlerEvent;

    let permissions = getPermissionsFromEvent(dummyEvent) as string[];
    expect(permissions.length).toBe(2);

    dummyEvent.requestContext = { authorizer: {} };
    let permissions3 = getPermissionsFromEvent(dummyEvent);
    expect(permissions3).toBe(false);

    delete dummyEvent.requestContext;
    let permissions2 = getPermissionsFromEvent(dummyEvent);
    expect(permissions2).toBe(false);
  });

  it('getAdminflagFromEvent() --> retorna a flag de admin do authorizer', () => {
    let dummyEvent = {
      pathParameters: {},
      headers: {},
      requestContext: {
        authorizer: {
          is_admin: 'TRUE'
        }
      }
    } as HandlerEvent;

    let isAdmin = getAdminflagFromEvent(dummyEvent);
    expect(isAdmin).toBe(true);

    delete dummyEvent.requestContext;
    isAdmin = getAdminflagFromEvent(dummyEvent);
    expect(isAdmin).toBe(false);
  });
});
