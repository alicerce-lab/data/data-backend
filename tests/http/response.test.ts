import { StatusCodes } from 'http-status-codes';
import { response, responseError } from '../../src/http/response';

describe('@http -> response', () => {
  it('response() --> retorna uma resposta com status e body', () => {
    const httpResponse = response({ data: 'teste' }, StatusCodes.CREATED);
    const expectedBody = '{ "data": "teste" }';
    expect(httpResponse.statusCode).toBe(StatusCodes.CREATED);
    expect(httpResponse.body.replace(/\s+/g, ' ').trim()).toBe(expectedBody);
  });

  it('response() --> retorna uma resposta com status padrao e body', () => {
    const httpResponse = response({ data: 'teste' });
    const expectedBody = '{ "data": "teste" }';
    expect(httpResponse.statusCode).toBe(StatusCodes.OK);
    expect(httpResponse.body.replace(/\s+/g, ' ').trim()).toBe(expectedBody);
  });

  it('responseError() --> retorna uma resposta com status e body', () => {
    responseError({ path: 'id', message: 'erro' });
    const httpResponse = responseError({ path: 'id', message: 'erro' }, StatusCodes.BAD_REQUEST);
    const expectedBody = '{ "path": "id", "message": "erro" }';
    expect(httpResponse.statusCode).toBe(StatusCodes.BAD_REQUEST);
    expect(httpResponse.body.replace(/\s+/g, ' ').trim()).toBe(expectedBody);
  });
});
