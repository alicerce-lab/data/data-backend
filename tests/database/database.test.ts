import { Connection, ConnectionOptions } from 'typeorm';
import { Database } from '../../src/database/database';

describe('database -> Database', () => {
  it('getConnection() -> Deve retornar uma conexão ao banco de dados', async () => {
    const dbOptions: ConnectionOptions = {
      type: 'sqlite',
      database: 'db.sqlite',
    };

    let conn = await Database.connect(dbOptions);
    await Database.connect(dbOptions);
    expect(conn).toBeInstanceOf(Connection);

    conn = await Database.connect(dbOptions, true);
    await Database.connect(dbOptions);
    expect(conn).toBeInstanceOf(Connection);

    await conn.close()
    await Database.connect(dbOptions);
  });
});
