import { createSandbox, SinonSandbox } from 'sinon';
import { HandlerEvent, MiddlewareContext, HandlerResponse } from '../../src/types/handler';
import { createStubLoggedUser } from '../utils/testUtils';
import { permissionChecker } from '../../src/middlewares/permissionChecker';

describe('Middlewares', () => {
  let sandbox: SinonSandbox;

  beforeEach(() => {
    sandbox = createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('permissionChecker --> Deve verificar se o usuário tem ou não permissão para continuar', async () => {
    const context = {
      end: () => null,
      prev: () => null
    } as MiddlewareContext;

    let dummyEvent = {
      pathParameters: {},
      headers: {},
      requestContext: {
        authorizer: {
          user: JSON.stringify({ id: -1 }),
          is_admin: 'TRUE'
        }
      }
    } as HandlerEvent;

    createStubLoggedUser(sandbox);
    const res = await permissionChecker(dummyEvent, context);
    expect(res).toBeUndefined();

    dummyEvent.requestContext = {
      authorizer: {
        user: JSON.stringify({ id: 1 }),
        is_admin: 'FALSE'
      }
    };

    const res2 = await permissionChecker(dummyEvent, context);
    expect(res2).toBeUndefined();

    sandbox.restore();
    dummyEvent.requestContext = { authorizer: {} };
    const res3 = (await permissionChecker(dummyEvent, context)) as HandlerResponse;
    expect(res3.statusCode).toBe(403);

    dummyEvent.requestContext = {
      authorizer: {
        user: JSON.stringify({ id: 1 }),
        is_admin: 'FALSE',
        permissions: JSON.stringify(['test/read'])
      }
    };
    process.env.permission = 'test/test';

    createStubLoggedUser(sandbox, { user: { id: 1 } as any });
    const res4 = (await permissionChecker(dummyEvent, context)) as HandlerResponse;
    expect(res4.statusCode).toBe(403);

    dummyEvent.requestContext = {
      authorizer: {
        user: JSON.stringify({ id: 1 }),
        is_admin: 'FALSE',
        permissions: JSON.stringify(['test/test'])
      }
    };

    const res5 = await permissionChecker(dummyEvent, context);
    expect(res5).toBeUndefined();

    delete process.env.permission;
    const res6 = await permissionChecker(dummyEvent, context);
    expect(res6).toBeUndefined();
  });
});
