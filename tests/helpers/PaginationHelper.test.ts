import { PaginationHelper } from '../../src/helpers/PaginationHelper';
import { PaginationOptions } from '../../src/types/pagination';

describe('Helpers --> PaginationHelper', () => {
  it('getSearchSchema -> Deve retornar schema padrão do Joi para paginação', async () => {
    const searchSchema = PaginationHelper.getSearchSchema();
    expect(searchSchema).not.toBeNull();
  });

  it('getPagination -> retorna os limit e offset para paginação', async () => {
    let options = {
      query: 'teste',
      page: '0',
      size: '10',
      order: 'teste',
      sort: 'DESC',
    } as PaginationOptions;

    PaginationHelper.getPagination({});
    const pagination = PaginationHelper.getPagination(options);
    expect(pagination.limit).toBe(10);
    expect(pagination.offset).toBe(0);
  });

  it('getOrder -> cria um order formatado a partir do sort e order', async () => {
    PaginationHelper.getOrder('teste');
    const order = PaginationHelper.getOrder('teste', 'DESC');
    expect(order.teste).toBe('DESC');
  });

  it('formatData -> formata os dados para um output de lista padrão', async () => {
    PaginationHelper.formatData([[1], 1], 1, '0');
    const formatted = PaginationHelper.formatData([[1], 1], 1);
    expect(formatted.totalItems).toBe(1);
    expect(formatted.totalPages).toBe(1);
    expect(formatted.currentPage).toBe(0);
  });
});
