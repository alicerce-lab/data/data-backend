import Joi from 'joi';
import { ValidationHelper } from '../../src/helpers/ValidationHelper';

describe('Helpers --> ValidationHelper', () => {
  it('validate -> Deve validar um schema do Joi', async () => {
    const schema = Joi.object({
      param: Joi.string().required()
    });

    let data = { param: 'teste' };

    var { error } = ValidationHelper.validate(schema, data);
    expect(error).toBeUndefined();

    data.param = 1 as any;
    var { error } = ValidationHelper.validate(schema, data);
    expect(error).not.toBeUndefined();
  });
});
