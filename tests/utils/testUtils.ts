import { createStubInstance, SinonSandbox, SinonStubbedInstance, StubbableType } from 'sinon';
import { Connection, EntityManager, Repository } from 'typeorm';
import { Database } from '../../src/database/database';
import * as request from '../../src/http/request';

interface CreateStubConnectionOptions {
  connection?: SinonStubbedInstance<Connection>;
  entityManager?: SinonStubbedInstance<EntityManager>;
  getRepository?: SinonStubbedInstance<any>;
  getCustomRepository?: SinonStubbedInstance<any>;
}

export const createStubConnection = (sandbox: SinonSandbox, options?: CreateStubConnectionOptions) => {
  let fakeConn = createStubInstance(Connection);
  let fakeEntityManager: any = createStubInstance(EntityManager);
  let fakeRepository: any = createStubInstance(Repository);
  let fakeCustomRepository: any = fakeRepository;

  if (options && options.connection) fakeConn = options.connection;
  if (options && options.entityManager) fakeEntityManager = options.entityManager;
  if (options && options.getRepository) fakeRepository = options.getRepository;
  if (options && options.getCustomRepository) fakeCustomRepository = options.getCustomRepository;

  fakeConn.transaction.callsFake(async (func: any) => func(fakeEntityManager));
  fakeConn.getRepository.returns(fakeRepository);
  fakeConn.getCustomRepository.returns(fakeCustomRepository);

  sandbox.stub(Database, 'connect').callsFake(async (a: any, b: any) => fakeConn as any);
};

interface CreateStubLoggedUserOptions {
  user?: StubbableType<User> | SinonStubbedInstance<User> | User;
}

export const createStubLoggedUser = (sandbox: SinonSandbox, options?: CreateStubLoggedUserOptions) => {
  let dummyUser = new User();
  dummyUser.id = -1;

  if (options && options.user) dummyUser = options.user as any;
  sandbox.stub(request, 'getUserFromEvent').callsFake(async (event: any) => dummyUser as any);
};
