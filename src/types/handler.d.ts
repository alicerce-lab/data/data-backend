import { StatusCodes } from 'http-status-codes';
import { Handler as AwsHandler, Context as AwsContext } from 'aws-lambda';
import { EntityManager } from 'typeorm';
import { User } from '../entities/auth/User';

export type Headers = Record<string, string>;

export interface ApiHandlerResponse {
  statusCode: StatusCodes;
  headers: Headers;
  body: string;
}

export interface ApiHandlerEvent {
  pathParameters: Record<string, string | number>;
  queryStringParameters?: Record<string, string | number>;
  headers: Headers;
  body?: string;
  requestContext?: {
    authorizer: Record<string, string>;
  };
}

export interface HandlerOptions {
  loggedUser?: User;
  entityManager: EntityManager;
}

type MiddlewareRes = any;

export type ApiHandler = AwsHandler<ApiHandlerEvent, ApiHandlerResponse>;

export type MiddlewareContext = Partial<AwsContext> & { end: () => void; prev: MiddlewareRes };

export type MiddlewareHandler = (event: ApiHandlerEvent, context: MiddlewareContext) => Promise<MiddlewareRes>;
