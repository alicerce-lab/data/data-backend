export interface GradeData {
    type: 'MAPA' | 'MINIMAPA' | 'DISSERTATIVE',
    zdpBlockName?: string,
    zdpBlockId?: number,
    skillGrades: {id: number, grade?: number}[],
    trailId: number
}
