export interface Address {
  address: string;
  city: string;
  uf: string;
}

export interface FixedTools {
  fixed_button_keys: string[];
}

export interface AcademicInfo {
  name: string;
  course_name: string;
  end_date: string;
}

export const SEX_MALE = 'male';
export const SEX_FEMALE = 'female';

// constraints de age group
export const G1 = 'G1'; // 8 anos ou menos
export const G2 = 'G2'; // 9 a 11 anos
export const G3 = 'G3'; // 12 a 14 anos
export const G4 = 'G4'; // 15 a 17 anos
export const G5 = 'G5'; // 18 a 24 anos
export const G6 = 'G6'; // 25 anos ou mais

export const AGE_GROUP_CONSTRAINTS = [G1, G2, G3, G4, G5, G6];

// grau de escolaridade
export const FUNDAMENTAL_COMPLETO = 'fundamental_completo';
export const FUNDAMENTAL_INCOMPLETO = 'fundamental_incompleto';
export const MEDIO_COMPLETO = 'medio_completo';
export const MEDIO_INCOMPLETO = 'medio_incompleto';
export const SUPERIOR_COMPLETO = 'superior_completo';
export const SUPERIOR_INCOMPLETO = 'superior_incompleto';

export const EDUCATION_LEVEL_CONSTRAINTS = [
  FUNDAMENTAL_COMPLETO,
  FUNDAMENTAL_INCOMPLETO,
  MEDIO_COMPLETO,
  MEDIO_INCOMPLETO,
  SUPERIOR_COMPLETO,
  SUPERIOR_INCOMPLETO
];

// dias da semana
export const SUNDAY = 'sun'; // Domingo
export const MONDAY = 'mon'; // Segunda
export const TUESDAY = 'tue'; // Terça
export const WEDNESDAY = 'wed'; // Quarta
export const THURSDAY = 'thu'; // Quinta
export const FRIDAY = 'fri'; // Sexta
export const SATURDAY = 'sat'; // Sábado

export const WEEK_DAYS_CONSTRAINTS = [SUNDAY, MONDAY, TUESDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY];

// turnos
export const SHIFT_MATUTINAL = 'matutinal';
export const SHIFT_VERPERTINE = 'vespertine';
export const SHIFT_NOCTURNAL = 'nocturnal';

export const SHIFT_CONSTRAINTS = [SHIFT_MATUTINAL, SHIFT_VERPERTINE, SHIFT_NOCTURNAL];
