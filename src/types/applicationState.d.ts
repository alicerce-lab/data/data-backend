export interface UserDTO {
    id: number
    user_id?: number
    lms_student?: number
    atributes?: {atribute: string, value: string}[]
}

export interface QuestionResponseDTO {
    questionId: number
    skillId: number
    blockId: number
    blockName: string
    blockOrder: number
    responseTime: number
    alternativeId?: number
    alternativeValue?: number
    text?: string
}


export interface ApplicationStateDTO {
  miniMapaData?: {
    miniMapaSkillsAndQuestions: {
      id: number
      questions: number
    }[]
    trailIds: number[] | []
    responses?: QuestionResponseDTO[]
    questionIds?: number[]
    currentQuestionId?: number
  }
  mapaData?: {
    zdp?: string
    flowOverride?: string
    birthDate: string
    trailId: number
    localVariables?: {
      responses: QuestionResponseDTO[]
      skillIDsToTestStak: number[]
      newZDP?: string
      flow: string
      currentBlock: string
      dominatedBlocks: string[]
      notDominatedBlocks: string[]
      wrongResponsesCounter: number
      correctResponsesCounter: number
      localBlockTest: boolean
    }
  }
  dissertativeData?: {
    zdp?: string
    birthDate: string
    trailId: number
    responses?: QuestionResponseDTO[]
    currentQuestionId?: number
  }
  browserData?: any
  type: 'MAPA' | 'MINIMAPA' | 'DISSERTATIVE'
  status: 'PENDING' | 'FINALIZED' | 'ONGOING'
  id: number
  createdAt?: string
  valideUntil?: string
  user?: UserDTO
}
