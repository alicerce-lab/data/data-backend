export interface CalendarData {
    week_days: string[]
    shift?: 'matutinal'|'verpertine' | 'nocturnal'
    start: string;
    end: string;
}
