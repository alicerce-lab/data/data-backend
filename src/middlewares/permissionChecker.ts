import { MiddlewareHandler } from '../types/handler';
import { getUserFromEvent, getAdminflagFromEvent, getPermissionsFromEvent } from '../http/request';
import { responseError } from '../http/response';

export const permissionChecker: MiddlewareHandler = async (event, context) => {
  let user = await getUserFromEvent(event);
  let isAdmin = getAdminflagFromEvent(event);

  if (!user) {
    context.end();
    return responseError({ path: 'user', message: 'You must be logged in to the server.' }, 403);
  }

  // usuário -1 = usuário root
  if (user.id == -1 || isAdmin) return;

  // endpoint sem validação de permissão
  const endpointPerm = process.env.permission ? process.env.permission : null;
  if (!endpointPerm) return;

  // endpoint com validação de permissão
  const userPermissions = getPermissionsFromEvent(event);
  if (!userPermissions || !userPermissions.includes(endpointPerm)) {
    context.end();
    return responseError({ path: 'user', message: 'User is not allowed to access this resource.' }, 403);
  }
};
