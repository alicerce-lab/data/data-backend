import { AnySchema } from 'joi';

interface formattedError {
  path: string | number;
  message: string;
}

export class ValidationHelper {
  static validate<T = any>(schema: AnySchema<T>, data: T) {
    let { value, error } = schema.validate(data, { abortEarly: false });

    let formattedError: formattedError[] | undefined;
    if (error) {
      formattedError = error.details.map((err) => {
        return {
          path: err.path[0],
          message: err.message,
        };
      });
    }

    return { value, error: formattedError };
  }
}
