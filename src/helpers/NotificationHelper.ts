// import { sendNotification } from '../services/NotificationService';
// import templates from './notifications_templates.json';
const SYSTEM_KEY = process.env.SYSTEM_KEY;

// NOTIFICATION TYPES


/**
 * Criar a mensagem final a partir do template e parametros
 * @param {*} template
 * @param {*} params
 * @returns
 */
const getMessage = (template: any, params: any[]) => {
  const paramsInTemplate = template.match(/{\d}/g);
  if (!paramsInTemplate) return template;
  let message = template;

  for (let param of paramsInTemplate) {
    const index = parseInt(param.match(/\d+/g)[0]);
    if (!params[index]) throw new Error(`index ${index} not found in params`);

    const regex = new RegExp(`\\${param.replace('}', '\\}')}`, 'g');
    message = message.replace(regex, params[index]);
  }

  return message;
};
