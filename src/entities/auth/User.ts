import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import { Profile } from './Profile';

@Entity({ schema: 'public', name: 'users' })
export class User {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ type: 'varchar', length: 100, nullable: false })
  first_name: string;

  @Column({ type: 'varchar', length: 100, nullable: false })
  last_name: string;

  @Column({ type: 'varchar', length: 255, unique: true, nullable: false })
  email: string;

  @Column({ type: 'varchar', length: 14, nullable: true })
  cpf: string;

  @Column({ type: 'varchar', length: 20, nullable: true })
  contact_phone: string;

  @Column({ type: 'varchar', length: 100, nullable: true })
  contact_email: string;

  @Column({ type: 'varchar', length: 18, nullable: true })
  salesforce_contact_id: string;

  @Column({ type: 'boolean', nullable: false, default: true })
  active: boolean;

  @Column({ type: 'varchar', length: 50, nullable: false })
  type: string;

  @Column()
  last_login: Date;

  @Column({ type: 'varchar', length: 15, nullable: true })
  last_login_address: string;

  @CreateDateColumn({ nullable: false, default: () => new Date().getTime() })
  created_at: Date;

  @UpdateDateColumn({ nullable: false, default: () => new Date().getTime() })
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;

  @ManyToMany(() => Profile)
  @JoinTable({
    name: 'user_profiles',
    joinColumn: { name: 'user_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'profile_id', referencedColumnName: 'id' }
  })
  profiles: Profile[];
}
