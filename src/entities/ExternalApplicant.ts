import { Column, CreateDateColumn, DeleteDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { User } from './auth/User';
import { Student } from './lms/Student';

@Entity({ name: 'external_applicants', schema: 'mapa4' })
export class ExternalApplicant {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ type: 'jsonb', nullable: false })
  attributes: any;

  @Column({ type: 'bigint', nullable: true })
  user_id: number;

  @Column({ type: 'bigint', nullable: true })
  lms_student_id: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;

  @OneToOne(() => Student)
  @JoinColumn({ name: 'lms_student_id' })
  student: Student;

  @OneToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user: User;
}
