import { Column, CreateDateColumn, DeleteDateColumn, Entity, JoinColumn, JoinTable, ManyToMany, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
// import { Application } from './Application';
// import { Question } from './Question';
// import { Skill } from './Skill';

@Entity({ name: 'answersheets', schema: 'mapa' })
export class Answersheet {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ type: 'varchar', length: 100, nullable: false })
  salesforce_id: string;

  @Column({ type: 'bigint', nullable: false })
  user_id: number;

  @Column({ type: 'varchar', length: 100, nullable: false })
  student_name: string;

  @Column({ type: 'jsonb', nullable: false })
  answer_data: any;

  @Column({ type: 'jsonb', nullable: false })
  grades_data: any;

  @Column({ type: 'jsonb', nullable: false })
  setup_data: {cluster_name: string};

//   @Column({ type: 'bigint', nullable: false })
//   question_id: number;

//   @Column({ type: 'varchar', length: 100, nullable: false })
//   answer_type: string;


//   @Column({ type: 'bigint', nullable: false })
//   application_id: number;

//   @Column({ type: 'bigint', nullable: false })
//   index: number;

//   @Column({ type: 'bigint', nullable: false })
//   time_delta: number;

//   @Column({ type: 'bigint', nullable: true })
//   value: number;

//   @Column({ type: 'text', nullable: true })
//   text: string;

//   @CreateDateColumn()
//   created_at: Date;

//   @UpdateDateColumn()
//   updated_at: Date;

//   @DeleteDateColumn()
//   deleted_at?: Date;

//   @OneToOne(() => Skill)
//   @JoinColumn({ name: 'skill_id' })
//   skill: Skill;

//   @OneToOne(() => Application)
//   @JoinColumn({ name: 'application_id' })
//   application: Application;

//   @OneToOne(() => Question)
//   @JoinColumn({ name: 'question_id' })
//   question: Question;
}
