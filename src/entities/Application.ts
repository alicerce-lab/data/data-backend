import { Column, CreateDateColumn, DeleteDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { ApplicationStateDTO } from '../types/applicationState';
import { User } from './auth/User';
import { ExternalApplicant } from './ExternalApplicant';
import { Student } from './lms/Student';
import { Trail } from './Trail';

@Entity({ name: 'applications', schema: 'mapa4' })
export class Application {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ type: 'bigint', nullable: true })
  external_applicant_id: number;

  @Column({ type: 'bigint', nullable: true })
  user_id: number;

  @Column({ type: 'bigint', nullable: true })
  lms_student_id: number;

  @Column({ type: 'varchar', length: 100, nullable: false })
  status: string;

  @Column({ type: 'jsonb', nullable: false })
  state: ApplicationStateDTO;

  @Column({ type: 'bigint', nullable: false })
  trail_id: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;

  @OneToOne(() => ExternalApplicant)
  @JoinColumn({ name: 'external_applicant_id' })
  externalApplicant: ExternalApplicant;

  @OneToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user: User;

  @OneToOne(() => Student)
  @JoinColumn({ name: 'lms_student_id' })
  student: Student;

  @OneToOne(() => Trail)
  @JoinColumn({ name: 'trail_id' })
  trail: Trail;
}
