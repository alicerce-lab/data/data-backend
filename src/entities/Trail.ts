import { Column, CreateDateColumn, DeleteDateColumn, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Block } from './Block';
import { Level } from './Level';

@Entity({ name: 'trails', schema: 'mapa4' })
export class Trail {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ type: 'varchar', length: 100, nullable: false })
  name: string;

  @Column({ type: 'bigint', default: 1 })
  version: number
  
  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;

  @ManyToMany(() => Level)
  @JoinTable({
    schema: 'mapa4',
    name: 'trails_levels',
    joinColumn: { name: 'trail_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'level_id' }
  })
  levels: Level[];

  @OneToMany(() => Block, (block) => block.trail_id)
  blocks: Block[];
}
