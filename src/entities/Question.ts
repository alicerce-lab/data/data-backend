import { Column, CreateDateColumn, DeleteDateColumn, Entity, JoinColumn, JoinTable, ManyToMany, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Skill } from './Skill';

@Entity({ name: 'questions', schema: 'mapa4' })
export class Question {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ type: 'bigint', nullable: false })
  skill_id: number;

  @Column({ type: 'varchar', length: 100, nullable: false })
  answer_type: string;

  @Column({ type: 'jsonb', nullable: false })
  answers_data: string;

  @Column({ type: 'jsonb', nullable: false })
  statement_data: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;

  @OneToOne(() => Skill)
  @JoinColumn({ name: 'skill_id' })
  skill: Skill;
}
