import { CalendarData } from 'src/types/class';
import {
  AfterLoad,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import { Place } from './Place';
import { Student } from './Student';

export const TYPE_CORE = 'core';
export const TYPE_ENGLISH = 'english';
export const TYPE_PREP_COURSE = 'prep_course';
export const TYPE_CLUB = 'club';

export const CLASS_TYPE_CONSTRAINTS = [TYPE_CORE, TYPE_ENGLISH, TYPE_PREP_COURSE, TYPE_CLUB];

export const STATUS_ACTIVE = 'active';
export const STATUS_INACTIVE = 'inactive';

export const CLASS_STATUS_CONSTRAINTS = [STATUS_ACTIVE, STATUS_INACTIVE];

@Entity({ name: 'classes', schema: 'lms' })
export class Class {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ type: 'varchar', length: 50, nullable: false })
  name: string;

  @Column({ type: 'varchar', length: 20, nullable: false })
  type: string;

  @Column({ type: 'date', nullable: true })
  start_date: Date;

  @Column({ type: 'jsonb', default: '[]', nullable: true })
  age_groups: string[];

  @Column({ type: 'jsonb', default: '[]', nullable: true })
  calendar_data: CalendarData[];

  @Column({ type: 'bigint', nullable: true })
  group_id: number;

  @Column({ type: 'bigint', nullable: true })
  place_id: number;

  @Column({ type: 'varchar', length: 50, nullable: false })
  status: string;

  @Column({ type: 'bigint', nullable: false })
  created_by: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;

  @ManyToMany(() => Student)
  @JoinTable({
    schema: 'lms',
    name: 'enrollments',
    joinColumn: { name: 'class_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'student_id' }
  })
  students: Student[];

  @ManyToOne(() => Place)
  @JoinColumn({ name: 'place_id' })
  place: Place;

  @Column({ select: false, insert: false, update: false })
  favorited: boolean;

  @Column({ select: false, insert: false, update: false })
  fixed: boolean;

  @AfterLoad()
  init() {
    if (!this.calendar_data) {
      this.calendar_data = [];
    }
  }
}
