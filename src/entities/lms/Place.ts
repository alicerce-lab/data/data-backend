import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
    OneToOne
  } from 'typeorm';
  import { Address } from '../../types/generic';
  

  
  @Entity({ name: 'places', schema: 'lms' })
  export class Place {
    @PrimaryGeneratedColumn({ type: 'bigint' })
    id: number;
  
    @Column({ type: 'varchar', length: 100, nullable: false })
    name: string;
  
    @Column({ type: 'varchar', length: 50, nullable: false })
    category: string;
  
    @Column({ type: 'text', nullable: true })
    description: string | null;
  
    @Column({ type: 'bigint', nullable: true })
    parent_id: number | null;
  
    @Column({ type: 'boolean', nullable: false, default: false })
    virtual_place: boolean;
  
    @Column({ type: 'text', nullable: true })
    place_url: string | null;
  
    @Column({ type: 'jsonb', nullable: true })
    address_data: Address | null;
  
    @Column({ type: 'bigint', nullable: true })
    created_by: number;
  
    @CreateDateColumn()
    created_at: Date;
  
    @UpdateDateColumn()
    updated_at: Date;
  
    @DeleteDateColumn()
    deleted_at?: Date;
  
  
    @OneToOne(() => Place)
    @JoinColumn({ name: 'parent_id' })
    parent: Place;
  
  
  

  
    @Column({ select: false, insert: false, update: false })
    favorited: boolean;
  
    @Column({ select: false, insert: false, update: false })
    fixed: boolean;
  }
  