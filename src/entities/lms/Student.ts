import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';

// constraints de relacionamento
export const RELATIONSHIP_B2C = 'B2C'; // BUSINESS TO CUSTOMER
export const RELATIONSHIP_B2B = 'B2B'; // BUSINESS TO BUSINESS
export const RELATIONSHIP_SCHOLARSHIP = 'scholarship'; // BOLSISTA
export const RELATIONSHIP_GRATUITY = 'gratuity'; // GRATUIDADE
export const RELATIONSHIP_ALIPASS = 'alipass'; // ALIPASS

export const STUDENT_RELATIONSHIP_CONSTRAINTS = [
  RELATIONSHIP_B2C,
  RELATIONSHIP_B2B,
  RELATIONSHIP_SCHOLARSHIP,
  RELATIONSHIP_GRATUITY,
  RELATIONSHIP_ALIPASS
];

// constraints de status
export const STATUS_ACTIVE = 'active'; // ATIVO
export const STATUS_WAITING_REG_CLASS = 'waiting reg class'; // AGUARDANDO TURMA REGULAR
export const STATUS_WAITING_EXP_CLASS = 'waiting exp class'; // AGUARDANDO TURMA EXPERIMENTAL
export const STATUS_EXPERIMENTATION = 'experimentation'; // EXPERIMENTAÇÃO
export const STATUS_UNCONVERTED_EXP = 'unconverted exp'; // NÃO CONVERTIDO A EXPERIMENTAÇÃO
export const STATUS_PAUSED = 'paused'; // PAUSADO
export const STATUS_DEFAULTER = 'defaulter'; // INADIMPLENTE
export const STATUS_QUITTER = 'quitter'; // DESISTENTE

export const STUDENT_STATUS_CONSTRAINTS = [
  STATUS_ACTIVE,
  STATUS_DEFAULTER,
  STATUS_EXPERIMENTATION,
  STATUS_PAUSED,
  STATUS_QUITTER,
  STATUS_UNCONVERTED_EXP,
  STATUS_WAITING_EXP_CLASS,
  STATUS_WAITING_REG_CLASS
];

@Entity({ name: 'students', schema: 'lms' })
export class Student {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ type: 'varchar', length: 100, nullable: false })
  name: string;

  @Column({ type: 'varchar', length: 10, nullable: false })
  gender: string;

  @Column({ type: 'varchar', length: 100, nullable: true })
  email: string;

  @Column({ type: 'varchar', length: 20, nullable: false })
  phone: string;

  @Column({ type: 'jsonb', nullable: true })
  address_data: any;

  @Column({ type: 'date', nullable: false })
  birthdate: Date;

  @Column({ type: 'boolean', nullable: false })
  literate: boolean;

  @Column({ type: 'varchar', length: 10, nullable: true })
  age_group: string;

  @Column({ type: 'varchar', length: 10, nullable: true })
  school_grade: string;

  @Column({ type: 'varchar', length: 100, nullable: true })
  alicerce_email: string;

  @Column({ type: 'date', nullable: true })
  activation_date: Date;

  @Column({ type: 'varchar', length: 20, nullable: false })
  relationship_type: string;

  @Column({ type: 'varchar', length: 20, nullable: false })
  status: string;

  @Column({ type: 'bigint', nullable: false })
  created_by: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;
}
