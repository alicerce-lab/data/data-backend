import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import { Level } from './Level';
import { Trail } from './Trail';

@Entity({ name: 'blocks', schema: 'mapa4' })
export class Block {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ type: 'varchar', length: 100, nullable: false })
  name: string;

  @Column({ type: 'bigint', nullable: false })
  order: number;

  @Column({ type: 'bigint', nullable: false })
  trail_id: number;

  @Column({ type: 'bigint', nullable: true })
  level_id: number;

  @Column({ type: 'float', default: 0.0, nullable: false })
  color_variation: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;

  @OneToOne(() => Level)
  @JoinColumn({ name: 'level_id' })
  level?: Level;

  @OneToOne(() => Trail)
  @JoinColumn({ name: 'trail_id' })
  trail: Trail;
}
