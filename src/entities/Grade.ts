import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';
import { GradeData } from '../types/grades';
import { Application } from './Application';
import { User } from './auth/User';
import { Block } from './Block';
import { ExternalApplicant } from './ExternalApplicant';
import { Student } from './lms/Student';
import { Trail } from './Trail';

@Entity({ name: 'grades', schema: 'mapa4' })
export class Grade {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ type: 'bigint', nullable: true })
  application_id: number;

  @Column({ type: 'bigint', nullable: true })
  external_applicant_id: number;

  @Column({ type: 'bigint', nullable: true })
  user_id: number;

  @Column({ type: 'bigint', nullable: true })
  lms_student_id: number;

  @Column({ type: 'jsonb', nullable: false })
  grade_data: GradeData;

  @Column({ type: 'bigint', nullable: false })
  trail_id: number;

  @Column({ type: 'boolean', default: false })
  is_current: boolean;

  @Column({ type: 'bigint', nullable: true })
  zdp_block_id: number;

  @Column({ type: 'bigint', nullable: true })
  meta_block_id: number;

  @Column({ type: 'boolean', nullable: false })
  is_meta_block_validated: boolean;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at?: Date;

  @OneToOne(() => Application)
  @JoinColumn({ name: 'application_id' })
  application: Application;

  @OneToOne(() => ExternalApplicant)
  @JoinColumn({ name: 'external_applicant_id' })
  externalApplicant: ExternalApplicant;

  @OneToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user: User;

  @OneToOne(() => Student)
  @JoinColumn({ name: 'lms_student_id' })
  student: Student;

  @OneToOne(() => Trail)
  @JoinColumn({ name: 'trail_id' })
  trail: Trail;

  @OneToOne(() => Block)
  @JoinColumn({ name: 'zdp_block_id' })
  zdp_block?: Block;

  @OneToOne(() => Block)
  @JoinColumn({ name: 'meta_block_id' })
  meta_block?: Block;
}
