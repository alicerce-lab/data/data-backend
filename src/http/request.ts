import { Database } from '../database/database';
import { HandlerEvent } from '../types/handler';

export const getUserFromEvent = async (event: HandlerEvent): Promise<false | User> => {
  if (!event.requestContext) return false;

  let user: any = event.requestContext.authorizer.user;
  if (!user) return false;

  user = JSON.parse(user);

  let conn = await Database.connect();
  const res = await conn.getRepository(User).findOne(user.id);
  
  if (!res) return false;

  return res;
};

export const getPermissionsFromEvent = (event: HandlerEvent) => {
  if (!event.requestContext) return false;

  const permissions = event.requestContext.authorizer.permissions;
  if (!permissions) return false;
  return JSON.parse(permissions) as string[];
};

export const getAdminflagFromEvent = (event: HandlerEvent) => {
  if (!event.requestContext) return false;
  const isAdmin = event.requestContext.authorizer.is_admin;
  return isAdmin === 'TRUE';
};
