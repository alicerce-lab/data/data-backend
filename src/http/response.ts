import { StatusCodes } from 'http-status-codes';
import { ApiHandlerResponse } from '../types/handler';

export type ErrorCodes =
  | StatusCodes.BAD_REQUEST
  | StatusCodes.NOT_FOUND
  | StatusCodes.UNAUTHORIZED
  | StatusCodes.FORBIDDEN
  | StatusCodes.INTERNAL_SERVER_ERROR;

export const response = (responseData: any, statusCode: StatusCodes = StatusCodes.OK): ApiHandlerResponse => {
  return {
    statusCode,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': 'true',
    },
    body: JSON.stringify(responseData, null, 2),
  };
};

export const responseError = (
  errorData: any,
  errorCode: StatusCodes = StatusCodes.INTERNAL_SERVER_ERROR
): ApiHandlerResponse => {
  return response(errorData, errorCode);
};
