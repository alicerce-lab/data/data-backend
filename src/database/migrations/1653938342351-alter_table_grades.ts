import { MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey } from 'typeorm';

/**
 * Alter table grades,
 * drop column block_id,
 * add column zdp_block_id,
 * add column meta_block_id
 */
export class alterTableGrades1653938342351 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('grades', 'block_id');
    await queryRunner.addColumns('grades', [
      new TableColumn({ name: 'zdp_block_id', type: 'bigint' }),
      new TableColumn({ name: 'meta_block_id', type: 'bigint' })
    ]);
    await queryRunner.createForeignKeys(new Table({ name: 'grades', schema: 'mapa4' }), [
      new TableForeignKey({
        columnNames: ['zdp_block_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'mapa4.blocks'
      }),
      new TableForeignKey({
        columnNames: ['meta_block_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'mapa4.blocks'
      })
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn('grades', new TableColumn({ name: 'block_id', type: 'bigint' }));
    await queryRunner.dropColumns('grades', [
      new TableColumn({ name: 'zdp_block_id', type: 'bigint' }),
      new TableColumn({ name: 'meta_block_id', type: 'bigint' })
    ]);
    await queryRunner.createForeignKey(
      'grades',
      new TableForeignKey({
        columnNames: ['block_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'mapa4.blocks'
      })
    );
  }
}
