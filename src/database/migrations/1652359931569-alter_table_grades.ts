import { Column, MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey } from 'typeorm';

/**
 * Alter table Grades
 * new column block_id
 */

export class alterTableGrades1652359931569 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      new Table({ name: 'grades', schema: 'mapa4' }),
      new TableColumn({ name: 'block_id', type: 'bigint', isNullable: false })
    );
    await queryRunner.createForeignKey(
      new Table({ name: 'grades', schema: 'mapa4' }),
      new TableForeignKey({
        columnNames: ['block_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'mapa4.blocks'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      new Table({ name: 'grades', schema: 'mapa4' }),
      new TableForeignKey({
        columnNames: ['block_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'mapa4.blocks'
      })
    );
    await queryRunner.dropColumn(
      new Table({ name: 'grades', schema: 'mapa4' }),
      new TableColumn({ name: 'block_id', type: 'bigint', isNullable: false })
    );
  }
}
