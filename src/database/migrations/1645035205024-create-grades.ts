import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm";

export class createGrades1645035205024 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'grades',
        schema: 'mapa4',
        columns: [
          { name: 'id', type: 'bigint', isPrimary: true, isGenerated: true },
          { name: 'application_id', type: 'bigint', isNullable: true },
          { name: 'external_applicant_id', type: 'bigint', isNullable: true },
          { name: 'user_id', type: 'bigint', isNullable: true },
          { name: 'lms_student_id', type: 'bigint', isNullable: true },
          { name: 'grade_data', type: 'jsonb', isNullable: false },
          { name: 'trail_id', type: 'bigint',  isNullable: false },
          { name: 'created_at', type: 'timestamp', default: 'now()', isNullable: false },
          { name: 'updated_at', type: 'timestamp', default: 'now()', isNullable: false },
          { name: 'deleted_at', type: 'timestamp', isNullable: true }
        ]
      }),
      true
    );

    await queryRunner.createForeignKey(
      'grades',
      new TableForeignKey({
        columnNames: ['trail_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'mapa4.trails'
      })
  );

    await queryRunner.createForeignKey(
      'grades',
      new TableForeignKey({
        columnNames: ['external_applicant_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'mapa4.external_applicants'
      })
    );

    await queryRunner.createForeignKey(
      'grades',
      new TableForeignKey({
        columnNames: ['user_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'public.users'
      })
    );

    await queryRunner.createForeignKey(
      'grades',
      new TableForeignKey({
        columnNames: ['lms_student_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'lms.students'
      })
    );

    await queryRunner.createForeignKey(
      'grades',
      new TableForeignKey({
        columnNames: ['application_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'mapa4.applications'
      })
    );
  }


  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(new Table({ name: 'grades', schema: 'mapa4' }));
  }

}
