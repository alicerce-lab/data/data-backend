import {MigrationInterface, QueryRunner, Table, TableColumn} from "typeorm";

export class updateTrails1646920351313 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumns(new Table({ schema: 'mapa4', name: 'trails' }), [
          new TableColumn({ name: 'version', type: 'bigint', default: 1 }),
        ]);
      }

      public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumns(new Table({ schema: 'mapa4', name: 'trails' }), [
          new TableColumn({ name: 'verion', type: 'bigint', default: 1 }),
        ]);
      }

}
