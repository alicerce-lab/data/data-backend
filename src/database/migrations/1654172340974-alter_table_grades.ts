import { MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey } from 'typeorm';

/**
 * Alter table grades
 * zpd_block_id nullable
 * meta_block_id nullable
 * new column is_meta_block_validate boolean default false
 */
export class alterTableGrades1654172340974 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      new Table({ name: 'grades', schema: 'mapa4' }),
      new TableColumn({ name: 'zdp_block_id', type: 'bigint', isNullable: false }),
      new TableColumn({ name: 'zdp_block_id', type: 'bigint', isNullable: true })
    );
    await queryRunner.changeColumn(
      new Table({ name: 'grades', schema: 'mapa4' }),
      new TableColumn({ name: 'meta_block_id', type: 'bigint', isNullable: false }),
      new TableColumn({ name: 'meta_block_id', type: 'bigint', isNullable: true })
    );
    await queryRunner.addColumn(
      new Table({ name: 'grades', schema: 'mapa4' }),
      new TableColumn({ name: 'is_meta_block_validated', type: 'boolean', default: false })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.changeColumn(
      new Table({ name: 'grades', schema: 'mapa4' }),
      new TableColumn({ name: 'zdp_block_id', type: 'bigint', isNullable: true }),
      new TableColumn({ name: 'zdp_block_id', type: 'bigint', isNullable: false })
    );
    await queryRunner.changeColumn(
      new Table({ name: 'grades', schema: 'mapa4' }),
      new TableColumn({ name: 'meta_block_id', type: 'bigint', isNullable: true }),
      new TableColumn({ name: 'meta_block_id', type: 'bigint', isNullable: false })
    );
    await queryRunner.dropColumn(
      new Table({ name: 'grades', schema: 'mapa4' }),
      new TableColumn({ name: 'is_meta_block_validated', type: 'boolean', default: false })
    );
  }
}
