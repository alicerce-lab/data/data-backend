import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createTrailsLevels1645033185360 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
          new Table({
            name: 'trails_levels',
            schema: 'mapa4',
            columns: [
              { name: 'id', type: 'bigint', isPrimary: true, isGenerated: true },
              { name: 'trail_id', type: 'bigint',  isNullable: false },
              { name: 'level_id', type: 'bigint',  isNullable: false },
              { name: 'created_at', type: 'timestamp', default: 'now()', isNullable: false },
              { name: 'updated_at', type: 'timestamp', default: 'now()', isNullable: false },
              { name: 'deleted_at', type: 'timestamp', isNullable: true }
            ]
          }),
          true
        );

        await queryRunner.createForeignKey(
            'trails_levels',
            new TableForeignKey({
              columnNames: ['trail_id'],
              referencedColumnNames: ['id'],
              referencedTableName: 'mapa4.trails'
            })
          );

        await queryRunner.createForeignKey(
          'trails_levels',
          new TableForeignKey({
            columnNames: ['level_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'mapa4.levels'
          })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable(new Table({ name: 'trails_levels', schema: 'mapa4' }));
      }

}
