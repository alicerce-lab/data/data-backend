import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createBlocks1645033811748 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
          new Table({
            name: 'blocks',
            schema: 'mapa4',
            columns: [
              { name: 'id', type: 'bigint', isPrimary: true, isGenerated: true },
              { name: 'name', type: 'varchar', length: '100', isNullable: false },
              { name: 'order', type: 'bigint', isNullable: false },
              { name: 'trail_id', type: 'bigint',  isNullable: false },
              { name: 'level_id', type: 'bigint',  isNullable: true },
              { name: 'created_at', type: 'timestamp', default: 'now()', isNullable: false },
              { name: 'updated_at', type: 'timestamp', default: 'now()', isNullable: false },
              { name: 'deleted_at', type: 'timestamp', isNullable: true }
            ]
          }),
          true
        );

        await queryRunner.createForeignKey(
            'blocks',
            new TableForeignKey({
              columnNames: ['trail_id'],
              referencedColumnNames: ['id'],
              referencedTableName: 'mapa4.trails'
            })
        );

        await queryRunner.createForeignKey(
          'blocks',
          new TableForeignKey({
            columnNames: ['level_id'],
            referencedColumnNames: ['id'],
            referencedTableName: 'mapa4.levels'
          })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable(new Table({ name: 'blocks', schema: 'mapa4' }));
    }

}
