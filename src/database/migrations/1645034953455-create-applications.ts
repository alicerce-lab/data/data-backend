import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm";

export class createApplications1645034953455 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'applications',
        schema: 'mapa4',
        columns: [
          { name: 'id', type: 'bigint', isPrimary: true, isGenerated: true },
          { name: 'external_applicant_id', type: 'bigint', isNullable: true },
          { name: 'user_id', type: 'bigint', isNullable: true },
          { name: 'lms_student_id', type: 'bigint', isNullable: true },
          { name: 'status', type: 'varchar', length: '100', isNullable: false },
          { name: 'state', type: 'jsonb', isNullable: false },
          { name: 'trail_id', type: 'bigint', isNullable: false },
          { name: 'created_at', type: 'timestamp', default: 'now()', isNullable: false },
          { name: 'updated_at', type: 'timestamp', default: 'now()', isNullable: false },
          { name: 'deleted_at', type: 'timestamp', isNullable: true }
        ]
      }),
      true
    );

    await queryRunner.createForeignKey(
      'applications',
      new TableForeignKey({
        columnNames: ['trail_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'mapa4.trails'
      })
    );

    await queryRunner.createForeignKey(
      'applications',
      new TableForeignKey({
        columnNames: ['external_applicant_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'mapa4.external_applicants'
      })
    );

    await queryRunner.createForeignKey(
      'applications',
      new TableForeignKey({
        columnNames: ['user_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'public.users'
      })
    );

    await queryRunner.createForeignKey(
      'applications',
      new TableForeignKey({
        columnNames: ['lms_student_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'lms.students'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(new Table({ name: 'applications', schema: 'mapa4' }));
  }

}
