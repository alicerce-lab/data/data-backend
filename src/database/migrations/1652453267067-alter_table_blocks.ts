import { MigrationInterface, QueryRunner, Table, TableColumn } from 'typeorm';

/**
 * Alter table blocks
 * add new column Color Variation
 */

export class alterTableBlocks1652453267067 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      new Table({ name: 'blocks', schema: 'mapa4' }),
      new TableColumn({ name: 'color_variation', type: 'float', default: 0.0 })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      new Table({ name: 'blocks', schema: 'mapa4' }),
      new TableColumn({ name: 'color_variation', type: 'float', default: 0.0 })
    );
  }
}
