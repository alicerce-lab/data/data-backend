import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createQuestions1645034638225 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
          new Table({
            name: 'questions',
            schema: 'mapa4',
            columns: [
              { name: 'id', type: 'bigint', isPrimary: true, isGenerated: true },
              { name: 'skill_id', type: 'bigint', isNullable: false },
              { name: 'answer_type', type: 'varchar', length: '100', isNullable: false },
              { name: 'answers_data', type: 'jsonb', isNullable: false },
              { name: 'statement_data', type: 'jsonb', isNullable: false },
              { name: 'created_at', type: 'timestamp', default: 'now()', isNullable: false },
              { name: 'updated_at', type: 'timestamp', default: 'now()', isNullable: false },
              { name: 'deleted_at', type: 'timestamp', isNullable: true }
            ]
          }),
          true
        );

        await queryRunner.createForeignKey(
            'questions',
              new TableForeignKey({
                  columnNames: ['skill_id'],
                  referencedColumnNames: ['id'],
                  referencedTableName: 'mapa4.skills'
              })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable(new Table({ name: 'questions', schema: 'mapa4' }));
    }

}
