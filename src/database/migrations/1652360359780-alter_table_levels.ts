import { MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey } from 'typeorm';

/**
 * Alter table levels
 * add new column color
 */

export class alterTableLevels1652360359780 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      new Table({ name: 'levels', schema: 'mapa4' }),
      new TableColumn({ name: 'color', type: 'varchar', length: '10', isNullable: true })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      new Table({ name: 'levels', schema: 'mapa4' }),
      new TableColumn({ name: 'color', type: 'varchar', length: '10', isNullable: true })
    );
  }
}
