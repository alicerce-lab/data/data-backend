import { MigrationInterface, QueryRunner, Table, TableColumn } from 'typeorm';

/**
 * Alter table Grades
 * Adding column is_current, boolean, default false
 */

export class alterTableGrades1652208903412 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      new Table({ name: 'grades', schema: 'mapa4' }),
      new TableColumn({ name: 'is_current', type: 'boolean', default: false })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn(
      new Table({ name: 'grades', schema: 'mapa4' }),
      new TableColumn({ name: 'is_current', type: 'boolean', default: false })
    );
  }
}
