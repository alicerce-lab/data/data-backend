import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm";

export class createAnswers1645035323372 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'answers',
        schema: 'mapa4',
        columns: [
          { name: 'id', type: 'bigint', isPrimary: true, isGenerated: true },
          { name: 'skill_id', type: 'bigint', isNullable: false },
          { name: 'question_id', type: 'bigint', isNullable: false },
          { name: 'index', type: 'bigint', isNullable: true },
          { name: 'answer_type', type: 'varchar', length: '100', isNullable: false },
          { name: 'application_id', type: 'bigint', isNullable: false },
          { name: 'time_delta', type: 'bigint', isNullable: false },
          { name: 'value', type: 'bigint', isNullable: true },
          { name: 'text', type: 'text', isNullable: true },
          { name: 'created_at', type: 'timestamp', default: 'now()', isNullable: false },
          { name: 'updated_at', type: 'timestamp', default: 'now()', isNullable: false },
          { name: 'deleted_at', type: 'timestamp', isNullable: true }
        ]
      }),
      true
    );

    await queryRunner.createForeignKey(
      'answers',
      new TableForeignKey({
        columnNames: ['skill_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'mapa4.skills'
      })
    );

    await queryRunner.createForeignKey(
      'answers',
      new TableForeignKey({
        columnNames: ['question_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'mapa4.questions'
      })
    );

    await queryRunner.createForeignKey(
      'answers',
      new TableForeignKey({
        columnNames: ['application_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'mapa4.applications'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(new Table({ name: 'answers', schema: 'mapa4' }));
  }

}
