import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createSkills1645034129035 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
          new Table({
            name: 'skills',
            schema: 'mapa4',
            columns: [
              { name: 'id', type: 'bigint', isPrimary: true, isGenerated: true },
              { name: 'block_id', type: 'bigint',  isNullable: false },
              { name: 'measurable', type: 'boolean', default: true, isNullable: false },
              { name: 'name', type: 'varchar', length: '100', isNullable: false },
              { name: 'descriptions', type: 'text', isNullable: false },
              { name: 'created_at', type: 'timestamp', default: 'now()', isNullable: false },
              { name: 'updated_at', type: 'timestamp', default: 'now()', isNullable: false },
              { name: 'deleted_at', type: 'timestamp', isNullable: true }
            ]
          }),
          true
        );

        await queryRunner.createForeignKey(
          'skills',
            new TableForeignKey({
                columnNames: ['block_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'mapa4.blocks'
          })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable(new Table({ name: 'skills', schema: 'mapa4' }));
    }

}
