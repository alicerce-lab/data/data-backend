import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm";

export class createExternalApplicants1645034839596 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'external_applicants',
        schema: 'mapa4',
        columns: [
          { name: 'id', type: 'bigint', isPrimary: true, isGenerated: true },
          { name: 'attributes', type: 'jsonb', isNullable: false },
          { name: 'user_id', type: 'bigint', isNullable: true },
          { name: 'lms_student_id', type: 'bigint', isNullable: true },
          { name: 'created_at', type: 'timestamp', default: 'now()', isNullable: false },
          { name: 'updated_at', type: 'timestamp', default: 'now()', isNullable: false },
          { name: 'deleted_at', type: 'timestamp', isNullable: true }
        ]
      }),
      true
    );

    await queryRunner.createForeignKey(
      'external_applicants',
      new TableForeignKey({
        columnNames: ['user_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'public.users'
      })
    );

    await queryRunner.createForeignKey(
      'external_applicants',
      new TableForeignKey({
        columnNames: ['lms_student_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'lms.students'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(new Table({ name: 'external_applicants', schema: 'mapa4' }));
  }

}
