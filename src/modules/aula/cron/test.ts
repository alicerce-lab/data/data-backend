import { database } from '../../../../ormconfig';
import { Database } from '../../../database/database';
import { Answer } from '../../../entities/Answer';
import { Application } from '../../../entities/Application';
import { Answersheet } from '../../../entities/mapa3/Answersheet';
import { AnswersheetRepository } from '../../../repositories/AnswerSheetRepository';

export async function test(params: any) {
    console.log("Ola Lambda!");
    const conn = await Database.connect();
    const applicationRepo = conn.getCustomRepository(AnswersheetRepository);
    const answs = await applicationRepo.getAnswersByClusterName('%COL%')
    const ultimosBlocos = answs.map(answer => {
        return {
            aluno: answer.student_name,
            bloco: answer.answer_data.pop().block_id
        }
    })
    console.log(ultimosBlocos);
}