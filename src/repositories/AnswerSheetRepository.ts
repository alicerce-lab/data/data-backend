import { EntityRepository, Repository, SelectQueryBuilder } from 'typeorm';
import { Answersheet } from '../entities/mapa3/Answersheet';
import { Skill } from '../entities/Skill';

 @EntityRepository(Answersheet)
 export class AnswersheetRepository extends Repository<Answersheet> {

    public async getAnswersByClusterName(name: string): Promise<Answersheet[]> {
        const queryBuilder = this.createQueryBuilder("answersheet")
        queryBuilder.where("answersheet.setup_data ->> 'cluster_name' like :name", {name:name})
        const res = await queryBuilder.getMany()
        return res
    }

    // public async getSkillsFromTrail(trailId: number): Promise<Skill[]> {
    //     const queryBuilder = this.createQueryBuilder("skill")
    //         .leftJoinAndSelect("skill.block", "block")
    //         .leftJoinAndSelect("block.trail", "trail")
    //         .where("skill.trail.id = :trailId", { trailId })
    //     return await queryBuilder.getMany()
    // }
}
