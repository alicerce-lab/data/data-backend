import { PaginationHelper } from '../helpers/PaginationHelper';
import { PaginationOptions, PaginationResponse } from '../types/pagination';
import { EntityRepository, Repository, SelectQueryBuilder } from 'typeorm';
import { Application} from '../entities/Application'
import { StatusCodes } from 'http-status-codes';
import { Skill } from '../entities/Skill';

/**
 * @class
 * Repositório de Aplicacoes
 */
 @EntityRepository(Skill)
 export class SkillRepository extends Repository<Skill> {


    public async getSkillsFromTrail(trailId: number): Promise<Skill[]> {
        const queryBuilder = this.createQueryBuilder("skill")
            .leftJoinAndSelect("skill.block", "block")
            .leftJoinAndSelect("block.trail", "trail")
            .where("skill.trail.id = :trailId", { trailId })
        return await queryBuilder.getMany()
    }
}
