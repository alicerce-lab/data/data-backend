import { PaginationOptions, PaginationResponse } from '../types/pagination';
import { EntityRepository, Repository, SelectQueryBuilder } from 'typeorm';
import { PaginationHelper } from '../helpers/PaginationHelper';
import { Application} from '../entities/Application'
import { ApplicationStateDTO } from '../types/applicationState';

export const STATUS_PENDING = 'PENDING';
export const STATUS_FINALIZED = 'FINALIZED';
export const STATUS_ONGOING = 'ONGOING';

export const APPLICATION_STATUS_CONSTRAINTS = [STATUS_PENDING, STATUS_FINALIZED, STATUS_ONGOING];

export const TYPE_MAPA = 'MAPA';
export const TYPE_MINIMAPA = 'MINIMAPA';
export const TYPE_DISSERTATIVE = 'DISSERTATIVE';

export const APPLICATION_TYPE_CONSTRAINTS = [TYPE_MAPA, TYPE_MINIMAPA, TYPE_DISSERTATIVE];

/**
 * @class
 * Repositório de Aplicacoes
 */

 @EntityRepository(Application)
 export class ApplicationRepository extends Repository<Application> {

  async syncApplicationState(applicationState: ApplicationStateDTO): Promise<Application | undefined>{
    const application = await this.findOne(applicationState.id)
    if (!application){
      return undefined;
    }
    const updated = this.save({...application, state: applicationState, status: applicationState.status})
    return updated
  }

    /**
   * Lista as applicações de forma paginada
   * @param options parametros de pesquisa e paginação
   * @returns
   */
     async listApplicationPaginated(options: Partial<PaginationOptions>, params?: any): Promise<PaginationResponse> {
        const qBuilder = this.createQueryBuilder('application');

        if (options.order) {
            qBuilder.orderBy(options.order, options.sort ? options.sort : 'ASC');
          }

          this.createWhere(qBuilder, { ...options, ...params });

          const { page, limit, offset } = PaginationHelper.getPagination(options);
          qBuilder.take(limit).skip(offset);

          const res = await qBuilder.getManyAndCount();
          return PaginationHelper.formatData(res, limit, page);
     }

    /**
    * Cria um where para pesquisa
    * @param qbuilder
    * @param params
    */
   
     private createWhere(qbuilder: SelectQueryBuilder<Application>, params?: any): void {
        if (params.external_applicant_id) {
          qbuilder.andWhere('application.external_applicant_id = :external_applicant_id', { external_applicant_id: params.external_applicant_id });
        }

        if (params.user_id) {
          qbuilder.andWhere('application.user_id = :user_id', { user_id: params.user_id });
        }

        if (params.lms_student_id) {
          qbuilder.andWhere('application.lms_student_id = :lms_student_id', { lms_student_id: params.lms_student_id });
        }

        if (params.status) {
            qbuilder.andWhere('application.status = :status', { status: params.status });
        }
    }
}
