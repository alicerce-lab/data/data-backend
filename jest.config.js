module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  rootDir: './',
  coverageDirectory: './.coverage',
  collectCoverage: true,
  collectCoverageFrom: ['**/*.ts', '!**/*.d.ts'],
  coveragePathIgnorePatterns: [
    '/node_modules/',
    '/.build/',
    '/.coverage/',
    '/.middleware/',
    '/src/entities',
    '/src/services',
    '/src/subscribers',
    '/src/repositories',
    '/src/database/migrations',
    '/src/database/seeds'
  ],
  resetMocks: true,
  moduleNameMapper: {
    'src/(.*)': '<rootDir>/src/$1',
    'tests/(.*)': '<rootDir>/tests/$1'
  }
};
