module.exports = () => {
  const fs = require("fs");
  const YAML = require("yamljs");

  const external = fs.readdirSync("./handlers/external");

  if (process.env.NODE_ENV == "production") {
    
    let index = external.indexOf("development-endpoints.yml");
    if (index > -1) external.splice(index, 1);

  }

  const externalRaw = external
    .map((f) => fs.readFileSync(`./handlers/external/${f}`, "utf8"))
    .map((raw) => YAML.parse(raw));

  return [...externalRaw].reduce((result, handler) => Object.assign(result, handler), {});
};
